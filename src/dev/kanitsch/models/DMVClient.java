package dev.kanitsch.models;


import java.util.Objects;

public class DMVClient implements java.io.Serializable {

    private StateID stateID;
    private String reasonForVisit;

    public DMVClient() {
        stateID = new StateID();
    }
    public DMVClient(String reason, StateID stateID) {
        super();
        this.reasonForVisit = reason;
        this.stateID = stateID;
    }

    public StateID getStateID() {
        return stateID;
    }

    public void setStateID(StateID stateID) {
        this.stateID = stateID;
    }

    public String getReasonForVisit() {
        return reasonForVisit;
    }

    public void setReasonForVisit(String reasonForVisit) {
        this.reasonForVisit = reasonForVisit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DMVClient dmvClient = (DMVClient) o;
        return Objects.equals(stateID, dmvClient.stateID) && Objects.equals(reasonForVisit, dmvClient.reasonForVisit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stateID, reasonForVisit);
    }
}
