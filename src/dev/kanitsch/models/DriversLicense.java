package dev.kanitsch.models;

import java.util.Objects;

public class DriversLicense extends StateID implements java.io.Serializable {

    private DriversLicenseType type;
    private int points;

    public DriversLicense() {
        super();
    }
    public DriversLicense(DriversLicenseType type, int points) {
        super();
        this.type = type;
        this.points = points;
    }
    public DriversLicense(int idNo, String name, String address, DriversLicenseType type, int points) {
        super(idNo, name, address);
        this.type = type;
        this.points = points;
    }

    public DriversLicenseType getType() {
        return type;
    }
    public void setType(DriversLicenseType type) {
        this.type = type;
    }
    public int getPoints() {
        return points;
    }
    public void setPoints(int points) {
        this.points = points;
    }
    @Override
    public String toString() {
        return "License {\n\tID#: " + getIdNo() +
                "\n\tName: " + getName() +
                "\n\tAddress: " + getAddress() +
                "\n\tLicense Type: " + type +
                "\n\tPoints: " + points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DriversLicense that = (DriversLicense) o;
        return points == that.points && type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type, points);
    }
}
