package dev.kanitsch.models;

public enum DriversLicenseType {

    CDL, GENERAL, MOTORCYCLE
}
