package dev.kanitsch.models;

public interface Authenticable {

    public default boolean authenticate() {
        return false;
    };

//    public int myAbstractMethod(String str);

}
