package dev.kanitsch.models;

import java.util.Objects;

public class StateID implements java.io.Serializable, Authenticable {

    private int idNo;
    private String name;
    private String address;

    public StateID() {
        super();
    }
    public StateID(int idNo, String name, String address) {
        setIdNo(idNo);
        this.address = address;
        this.name = name;
    }

    public int getIdNo() {
        return idNo;
    }
    public void setIdNo(int idNo) {
        if (idNo > 0)
            this.idNo = idNo;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ID#: " + idNo + "\n\tName: " + name + "\n\tAddress: " + address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StateID stateID = (StateID) o;
        return idNo == stateID.idNo && Objects.equals(name, stateID.name) && Objects.equals(address, stateID.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNo, name, address);
    }

    @Override
    public boolean authenticate() {
        if (idNo > 0) {
            System.out.println("This ID is authentic");
            return true;
        }
        else {
            System.out.println("This ID is PHONY!");
            return false;
        }
    }
}
