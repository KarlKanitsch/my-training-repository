package dev.kanitsch;

import dev.kanitsch.models.*;

public class DMVAPPDriver {

    public static void main(String[] args) {
        StateID id1 = new StateID(245,"John Smith","123 Main St. Tulsa, OK 11111");
//        System.out.println(id1.toString());

        DriversLicense dl1 = new DriversLicense(246,"Jane Smith","123 Main St. Tulsa, OK 11111", DriversLicenseType.GENERAL,6);
//        System.out.println(dl1.toString());

        id1.authenticate();
        DMVClient client1 = new DMVClient("Driver Test", id1);
        DMVClient client2 = new DMVClient("Renew License", dl1);

        System.out.println("Info for Client1");
        System.out.println(client1.getStateID().toString());
        System.out.println(client1.getReasonForVisit());
        System.out.println("\nInfo for Client2");
        System.out.println(client2.getStateID().toString());
        System.out.println(client2.getReasonForVisit());
    }

}
